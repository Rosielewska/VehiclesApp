package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.parkmobile.vehiclesapp.R;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private ArrayList<String> vehicleList;
    private final PublishSubject<Integer> onClickSubject = PublishSubject.create();

    public DataAdapter(ArrayList<String> vehicleList) {
        this.vehicleList = vehicleList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_cardview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textViewVehicleName.setText(vehicleList.get(position));

        holder.mainFrameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }

    public Observable<Integer> getPositionClicks() {
        return onClickSubject;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewVehicleName;
        private FrameLayout mainFrameLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewVehicleName = (TextView) itemView.findViewById(R.id.textviewVehicleName);
            mainFrameLayout = (FrameLayout) itemView.findViewById(R.id.mainFrameLayout);

        }
    }
}