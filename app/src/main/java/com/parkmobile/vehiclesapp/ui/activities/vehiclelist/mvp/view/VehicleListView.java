package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.parkmobile.vehiclesapp.jsondownload.objectentity.Vehicle;

import java.util.ArrayList;

import io.reactivex.Observer;

public interface VehicleListView {
    void setVehicleList(ArrayList<String> data);

    void showDetailFragment(Bundle bundle);

    void setAdapter(ArrayList<String> data, Observer observer);

    void showErrorMsg();

}
