package com.parkmobile.vehiclesapp.ui.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.parkmobile.vehiclesapp.R;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.fragment.VehicleListFragment;
import com.parkmobile.vehiclesapp.utils.Log;


public class VehicleListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            VehicleListFragment listvehicleFragment = new VehicleListFragment();
            setFragment(listvehicleFragment, "vehicleList", false);
        }
    }

    public void setFragment(Fragment fragment, String addToBackStackText, boolean addToBackStack) {
        Log.d("setFragment" + fragment);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        if (addToBackStack)
            transaction.addToBackStack(addToBackStackText);
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }
}
