package com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.view;

public interface VehicleDetailFragmentView {

    void setVehicleName(String name);

    void setVehicleId(String id);

    void setVehicleCountry(String country);

    void setVehicleColor(String color);

    void setVehicleType(String type);

    void setVehicleDefault(String vehicleDefault);
}
