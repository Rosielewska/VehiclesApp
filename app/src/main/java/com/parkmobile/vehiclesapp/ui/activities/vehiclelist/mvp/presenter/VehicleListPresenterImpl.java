package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.presenter;

import android.os.Bundle;

import com.parkmobile.vehiclesapp.conf.Conf;
import com.parkmobile.vehiclesapp.jsondownload.objectentity.Vehicle;
import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.fragment.VehicleDetailFragment;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.fragment.VehicleListFragment;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.interactor.VehicleListInteractor;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.view.VehicleListView;


import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class VehicleListPresenterImpl implements VehicleListPresenter, VehicleListInteractor.VehicleListInteractorCallback, Observer {

    private VehicleListView vehicleListView;
    private VehicleListInteractor interactor;
    private ArrayList<Vehicle> vehicleArrayList = new ArrayList<>();

    public VehicleListPresenterImpl(VehicleListView vehicleListView, VehicleListInteractor interactor) {
        this.vehicleListView = vehicleListView;
        this.interactor = interactor;

    }

    @Override
    public void getVehicleList() {
        interactor.getVehicleList(Conf.VEHICLE_URL, this);
    }

    @Override
    public void setData(Bundle bundle) {
        if (bundle != null) {
            vehicleArrayList = bundle.getParcelableArrayList(VehicleListFragment.VEHICLES_BUNDLE);
        } else {
            getVehicleList();
        }
        vehicleListView.setAdapter(getNameList(vehicleArrayList), this);
    }

    @Override
    public void returnVehicleList(ArrayList<Vehicle> vehicleArrayList) {

        vehicleListView.setVehicleList(getNameList(vehicleArrayList));
        this.getVehicleArrayList().clear();
        this.getVehicleArrayList().addAll(vehicleArrayList);

    }

    @Override
    public void connectionError() {
        vehicleListView.showErrorMsg();
    }

    private ArrayList<String> getNameList(ArrayList<Vehicle> vehicleArrayList) {
        ArrayList<String> nameList = new ArrayList<>();
        for (Vehicle vehicle : vehicleArrayList) {
            nameList.add(vehicle.getVrn());
        }
        return nameList;
    }

    private void showVehicleDetailFragment(int position) {

        if (getVehicleArrayList().size() > position) {
            Bundle args = new Bundle();
            args.putParcelable(VehicleDetailFragment.VEHICLE_DETAIL, getVehicleArrayList().get(position));
            vehicleListView.showDetailFragment(args);
        }
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(Object o) {
        Integer clickedPosition = (Integer) o;
        showVehicleDetailFragment(clickedPosition);
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }

    public ArrayList<Vehicle> getVehicleArrayList() {
        return vehicleArrayList;
    }
}
