package com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.presenter;

import android.os.Bundle;

public interface VehicleDetailPresenter {
    void setArguments(Bundle bundle);
}
