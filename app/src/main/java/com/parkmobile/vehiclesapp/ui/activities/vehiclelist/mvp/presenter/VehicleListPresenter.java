package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.presenter;

import android.os.Bundle;

import com.parkmobile.vehiclesapp.jsondownload.objectentity.Vehicle;

import java.util.ArrayList;

import io.reactivex.Observer;

public interface VehicleListPresenter {
    void getVehicleList();

    void setData(Bundle bundle);

    ArrayList<Vehicle> getVehicleArrayList();
}
