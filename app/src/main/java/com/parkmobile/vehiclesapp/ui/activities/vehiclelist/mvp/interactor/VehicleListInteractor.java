package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.interactor;

import com.parkmobile.vehiclesapp.jsondownload.objectentity.Vehicle;

import java.util.ArrayList;

public interface VehicleListInteractor {
    void getVehicleList(String url, VehicleListInteractorCallback vehicleListInteractorCallback);

    interface VehicleListInteractorCallback {
        void returnVehicleList(ArrayList<Vehicle> vehicleArrayList);

        void connectionError();
    }
}
