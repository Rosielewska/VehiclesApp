package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.interactor;

import com.parkmobile.vehiclesapp.jsondownload.JSONResponse;
import com.parkmobile.vehiclesapp.jsondownload.RequestInterface;
import com.parkmobile.vehiclesapp.utils.Log;

import java.util.ArrayList;
import java.util.Arrays;


import retrofit2.Call;
import retrofit2.Callback;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VehicleListInteractorImpl implements VehicleListInteractor {

    @Override
    public void getVehicleList(String url, final VehicleListInteractorCallback vehicleListInteractorCallback) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<JSONResponse> call = request.getJSON();
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();

                if (jsonResponse != null) {
                    if (vehicleListInteractorCallback != null) {
                        vehicleListInteractorCallback.returnVehicleList(new ArrayList<>(Arrays.asList(jsonResponse.getVehicles())));
                    }
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {

                Log.e("request", t);
                if (vehicleListInteractorCallback != null) {
                    vehicleListInteractorCallback.connectionError();
                }
            }
        });
    }


}
