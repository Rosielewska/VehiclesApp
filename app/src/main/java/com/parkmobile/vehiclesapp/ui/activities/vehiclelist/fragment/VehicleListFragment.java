package com.parkmobile.vehiclesapp.ui.activities.vehiclelist.fragment;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.support.design.widget.Snackbar;

import com.parkmobile.vehiclesapp.ui.activities.VehicleListActivity;
import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.fragment.VehicleDetailFragment;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.adapter.DataAdapter;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.interactor.VehicleListInteractorImpl;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.presenter.VehicleListPresenter;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.presenter.VehicleListPresenterImpl;
import com.parkmobile.vehiclesapp.ui.activities.vehiclelist.mvp.view.VehicleListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;

import com.parkmobile.vehiclesapp.R;


public class VehicleListFragment extends Fragment implements VehicleListView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.card_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.root)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private VehicleListPresenter presenter;
    private DataAdapter adapter;
    private ArrayList<String> data = new ArrayList<>();
    private VehicleDetailFragment vehicleDetailFragment;

    public static String VEHICLES_BUNDLE = "vehicles";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
        ButterKnife.bind(this, view);
        initViews();

        presenter = new VehicleListPresenterImpl(this, new VehicleListInteractorImpl());
        presenter.setData(savedInstanceState);

        return view;

    }

    private void initViews() {

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary);

    }

    @Override
    public void setAdapter(ArrayList<String> data, Observer observer) {
        this.data.addAll(data);
        adapter = new DataAdapter(this.data);
        recyclerView.setAdapter(adapter);
        adapter.getPositionClicks().subscribe(observer);

    }

    @Override
    public void showErrorMsg() {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, getResources().getString(R.string.connection_error), Snackbar.LENGTH_LONG);
        snackbar.show();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void setVehicleList(ArrayList<String> data) {
        this.data.clear();
        this.data.addAll(data);
        adapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(VEHICLES_BUNDLE, presenter.getVehicleArrayList());
    }

    @Override
    public void showDetailFragment(Bundle bundle) {
        if (vehicleDetailFragment == null)
            vehicleDetailFragment = new VehicleDetailFragment();
        vehicleDetailFragment.setArguments(bundle);
        ((VehicleListActivity) getActivity()).setFragment(vehicleDetailFragment, "detail", true);
    }

    @Override
    public void onRefresh() {
        presenter.getVehicleList();
    }
}

