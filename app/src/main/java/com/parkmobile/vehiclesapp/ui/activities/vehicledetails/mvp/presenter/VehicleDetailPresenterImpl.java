package com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.presenter;

import android.os.Bundle;

import com.parkmobile.vehiclesapp.jsondownload.objectentity.Vehicle;
import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.fragment.VehicleDetailFragment;
import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.view.VehicleDetailFragmentView;

public class VehicleDetailPresenterImpl implements VehicleDetailPresenter {

    private VehicleDetailFragmentView vehicleDetailFragmentView;

    public VehicleDetailPresenterImpl(VehicleDetailFragmentView vehicleDetailFragmentView) {
        this.vehicleDetailFragmentView = vehicleDetailFragmentView;
    }

    private void setVehicleDetails(Vehicle vehicle) {

        vehicleDetailFragmentView.setVehicleName(vehicle.getVrn());
        vehicleDetailFragmentView.setVehicleId(vehicle.getVehicleId() + "");
        vehicleDetailFragmentView.setVehicleCountry(vehicle.getCountry());
        vehicleDetailFragmentView.setVehicleType(vehicle.getType());
        vehicleDetailFragmentView.setVehicleColor(vehicle.getColor());
        vehicleDetailFragmentView.setVehicleDefault(vehicle.isDefaultVehicle() ? "true" : "false");

    }

    @Override
    public void setArguments(Bundle bundle) {
        if (bundle != null) {
            Vehicle vehicle = (Vehicle) bundle.getParcelable(VehicleDetailFragment.VEHICLE_DETAIL);
            if (vehicle != null) {
                setVehicleDetails(vehicle);
            }
        }
    }
}
