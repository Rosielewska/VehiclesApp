package com.parkmobile.vehiclesapp.ui.activities.vehicledetails.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parkmobile.vehiclesapp.R;

import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.presenter.VehicleDetailPresenter;
import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.presenter.VehicleDetailPresenterImpl;
import com.parkmobile.vehiclesapp.ui.activities.vehicledetails.mvp.view.VehicleDetailFragmentView;


import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleDetailFragment extends Fragment implements VehicleDetailFragmentView {

    @BindView(R.id.vehicle_name)
    TextView textViewName;
    @BindView(R.id.vehicle_id)
    TextView textViewId;
    @BindView(R.id.vehicle_country)
    TextView textViewCountry;
    @BindView(R.id.vehicle_color)
    TextView textViewColor;
    @BindView(R.id.vehicle_type)
    TextView textViewType;
    @BindView(R.id.vehicle_default)
    TextView textViewDefault;

    public static String VEHICLE_DETAIL = "vehicleDetails";
    private VehicleDetailPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_vehicle_details, container, false);
        ButterKnife.bind(this, view);

        presenter = new VehicleDetailPresenterImpl(this);
        presenter.setArguments(getArguments());

        return view;

    }

    @Override
    public void setVehicleName(String name) {
        textViewName.setText(name);
    }

    @Override
    public void setVehicleId(String id) {
        textViewId.setText(Html.fromHtml(String.format(getResources().getString(R.string.vehicle_detail_id), id)));
    }

    @Override
    public void setVehicleCountry(String country) {
        textViewCountry.setText(Html.fromHtml(String.format(getResources().getString(R.string.vehicle_detail_country), country)));
    }

    @Override
    public void setVehicleColor(String color) {
        textViewColor.setText(Html.fromHtml(String.format(getResources().getString(R.string.vehicle_detail_color), color)));
    }

    @Override
    public void setVehicleType(String type) {
        textViewType.setText(Html.fromHtml(String.format(getResources().getString(R.string.vehicle_detail_type), type)));
    }

    @Override
    public void setVehicleDefault(String vehicleDefault) {
        textViewDefault.setText(Html.fromHtml(String.format(getResources().getString(R.string.vehicle_detail_default), vehicleDefault)));
    }
}
