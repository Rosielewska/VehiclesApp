package com.parkmobile.vehiclesapp.jsondownload.objectentity;

import android.os.Parcel;
import android.os.Parcelable;

public class Vehicle implements Parcelable {

    private int vehicleId;
    private String vrn;
    private String country;
    private String color;
    private String type;
    private boolean defaultVehicle;


    public Vehicle(int vehicleId, String vrn, String country, String color,String type, boolean defaultVehicle) {
        setVehicleId(vehicleId);
        setVrn(vrn);
        setCountry(country);
        setColor(color);
        setType(type);
        setDefaultVehicle(defaultVehicle);
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVrn() {
        return vrn;
    }

    public void setVrn(String vrn) {
        this.vrn = vrn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    protected Vehicle(Parcel in) {
            vehicleId = in.readInt();
            vrn = in.readString();
            country = in.readString();
            color = in.readString();
            type=in.readString();
            defaultVehicle= in.readByte() != 0;
    }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(vehicleId);
            dest.writeString(vrn);
            dest.writeString(country);
            dest.writeString(color);
            dest.writeString(type);
            dest.writeByte((byte) (defaultVehicle ? 1 : 0));
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Vehicle> CREATOR = new Parcelable.Creator<Vehicle>() {
            @Override
            public Vehicle createFromParcel(Parcel in) {
                return new Vehicle(in);
            }

            @Override
            public Vehicle[] newArray(int size) {
                return new Vehicle[size];
            }
        };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isDefaultVehicle() {
        return defaultVehicle;
    }

    public void setDefaultVehicle(boolean defaultVehicle) {
        this.defaultVehicle = defaultVehicle;
    }
}