package com.parkmobile.vehiclesapp.jsondownload;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RequestInterface {
    @GET("vehicles")
    Call<JSONResponse> getJSON();
}
