package com.parkmobile.vehiclesapp;

import android.app.Application;

import com.parkmobile.vehiclesapp.conf.Conf;

import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Created by Rosielewska Justyna on 2018-06-15.
 */

public class App extends Application {

    @Override
    public void onCreate() {

        super.onCreate();
        Conf.setDEBUG(BuildConfig.DEBUG);
    }
}
