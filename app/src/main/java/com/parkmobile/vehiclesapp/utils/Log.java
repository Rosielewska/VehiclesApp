package com.parkmobile.vehiclesapp.utils;


import java.io.PrintWriter;
import java.io.StringWriter;

import com.parkmobile.vehiclesapp.conf.Conf;


public class Log {

    private static String TAG = "vehiclesApp";

    public static void d(String msg) {
        if (Conf.DEBUG) {
            if (msg != null)
                android.util.Log.d(TAG, msg);
        }
    }

    public static void e(String msg, Throwable ex) {
        if (Conf.DEBUG) {
            if (msg != null && ex != null) {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                android.util.Log.e(TAG, msg + ": " + sw.toString());
            }
        }
    }


}
